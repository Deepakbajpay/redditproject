package com.streamkast.xperience.ui.home;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.streamkast.xperience.R;
import com.streamkast.xperience.data.network.comments.CommentItem;
import com.streamkast.xperience.data.network.comments.CommentsResponse;
import com.streamkast.xperience.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {
    private final Context context;
    private List<CommentsResponse> data;
    private int itemViewWidth = 0;

    public CommentsAdapter(Context context) {
        data = new ArrayList<>();
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemViewWidth = parent.getWidth();
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_feed_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CommentItem commentItem = data.get(position).getData();
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ConstraintLayout.LayoutParams layoutParams1 = new ConstraintLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
        if (commentItem == null)
            return;
        CommentsAdapter commentsAdapter;
        if (commentItem.getDepth() == 0) {
            layoutParams.leftMargin = (int) Utils.convertDpToPixel(0, holder.itemView.getContext());
            layoutParams1.leftMargin = (int) Utils.convertDpToPixel(0, holder.itemView.getContext());

            holder.leftStrip.setBackgroundColor(Color.parseColor("#008577"));
        } else if (commentItem.getDepth() == 1) {
            holder.leftStrip.setBackgroundColor(Color.parseColor("#D81B60"));
            layoutParams.leftMargin = (int) Utils.convertDpToPixel(16, holder.itemView.getContext());
            layoutParams1.leftMargin = (int) Utils.convertDpToPixel(16, holder.itemView.getContext());
        } else if (commentItem.getDepth() == 2) {
            holder.leftStrip.setBackgroundColor(Color.parseColor("#FFFFFF"));
            layoutParams.leftMargin = (int) Utils.convertDpToPixel(32, holder.itemView.getContext());
            layoutParams1.leftMargin = (int) Utils.convertDpToPixel(32, holder.itemView.getContext());
        }

        if (commentItem.getReplies()!=null&&commentItem.getReplies().getData()!=null){
            System.out.println("CommentsAdapter.onBindViewHolder innerAdapter");
            holder.commentsRv.setLayoutManager(new LinearLayoutManager(context));
            commentsAdapter = new CommentsAdapter(context);
            commentsAdapter.setData(commentItem.getReplies().getData().getChildren());
            holder.commentsRv.setAdapter(commentsAdapter);
        }
//        holder.titleTv.setLayoutParams(layoutParams);
//        holder.leftStrip.setLayoutParams(layoutParams1);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.titleTv.setText(Html.fromHtml(commentItem.getBody(),Html.FROM_HTML_MODE_COMPACT));//commentItem.getBody());
        }else
            holder.titleTv.setText(Html.fromHtml(commentItem.getBody()));//commentItem.getBody());
    }

    @Override
    public int getItemCount() {
        if (data.size()>5)
            return 5;
        return data.size();
    }

    public void setData(List<CommentsResponse> data) {
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public interface ClickListener {
        void launchIntent(String filmName);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView titleTv, commentCountTv, likeCountTv, postedByTv, timeAgoTv, subredditTv;
        private ImageView thumbIv;
        private RecyclerView commentsRv;
        private View leftStrip;

        ViewHolder(View itemView) {
            super(itemView);
            commentsRv = itemView.findViewById(R.id.comments_rv);
            titleTv = itemView.findViewById(R.id.comment_title_tv);
            leftStrip = itemView.findViewById(R.id.left_strip);
        }
    }
}
