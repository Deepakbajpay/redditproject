package com.streamkast.xperience.ui.home;

import com.streamkast.xperience.data.network.comments.CommentsResponse;
import com.streamkast.xperience.data.network.feed_listing.ListingResponseDataChildrenItem;

import java.util.List;

public interface HomeFragmentContract {
    interface Presenter{
        void getFeedItems();
        void getFeedComments(String subreddit,String articleId);
    }
    interface View {
        void showFeedItems(List<ListingResponseDataChildrenItem> feedItems);
        void showComments(List<CommentsResponse> commentsResponseData);
        void showMessage(String message);
    }
}
