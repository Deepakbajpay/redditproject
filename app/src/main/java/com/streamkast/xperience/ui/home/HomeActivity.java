package com.streamkast.xperience.ui.home;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.streamkast.xperience.R;
import com.streamkast.xperience.ui.base.BaseActivity;

public class HomeActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        getSupportFragmentManager().beginTransaction().add(R.id.home_frag_container,

                new HomeFragment()).commit();
    }

    public void replaceFragment(Fragment fragment, String prevFragTag) {
        if (prevFragTag == null)
            getSupportFragmentManager().beginTransaction().replace(R.id.home_frag_container,
                    fragment).commit();
        else getSupportFragmentManager().beginTransaction().replace(R.id.home_frag_container,
                fragment, fragment.getClass().getName()).addToBackStack(prevFragTag).commit();
    }
}
