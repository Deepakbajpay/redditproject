package com.streamkast.xperience.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.streamkast.xperience.R;
import com.streamkast.xperience.data.network.comments.CommentsResponse;
import com.streamkast.xperience.data.network.feed_listing.FeedObjectDataItem;
import com.streamkast.xperience.data.network.feed_listing.ListingResponseDataChildrenItem;
import com.streamkast.xperience.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HomeFragment extends BaseHomeFragment implements CommentsAdapter.ClickListener, HomeFragmentContract.View {

    public CommentsAdapter commentsAdapter;
    HomeFragmentContract.Presenter presenter;
    private RecyclerView feedRv;
    private TextView titleTv, commentCountTv, likeCountTv, postedByTv, timeAgoTv, subredditTv;
    private ImageView thumbIv;
    private int itemViewWidth;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        presenter = new HomeFragmentPresenterImpl(this);

        feedRv = rootView.findViewById(R.id.home_rv);
        feedRv.setLayoutManager(new LinearLayoutManager(getHomeActivity()));
        commentsAdapter = new CommentsAdapter(getHomeActivity());
        feedRv.setAdapter(commentsAdapter);

        feedRv.setNestedScrollingEnabled(true);
        titleTv = rootView.findViewById(R.id.feed_item_title_tv);
        thumbIv = rootView.findViewById(R.id.feed_item_thumb_iv);
        commentCountTv = rootView.findViewById(R.id.feed_item_comment_count_tv);
        likeCountTv = rootView.findViewById(R.id.feed_item_likes_count_tv);
        postedByTv = rootView.findViewById(R.id.feed_item_posted_by_tv);
        timeAgoTv = rootView.findViewById(R.id.feed_item_time_ago_tv);
        subredditTv = rootView.findViewById(R.id.feed_item_subreddit_tv);

        presenter.getFeedItems();

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        itemViewWidth = view.getWidth();
    }

    @Override
    public void launchIntent(String url) {
    }

    @Override
    public void showFeedItems(List<ListingResponseDataChildrenItem> feedItems) {
        if (feedItems != null) {
            FeedObjectDataItem feedObjectDataItem = feedItems.get(0).getData();
            titleTv.setText(feedObjectDataItem.getTitle());
            postedByTv.setText(feedObjectDataItem.getAuthor());
            timeAgoTv.setText(Utils.getTimeAgo(feedObjectDataItem.getCreated()));
            subredditTv.setText(feedObjectDataItem.getSubredditNamePrefixed());

            if (feedObjectDataItem.getNumComments() != null)
                commentCountTv.setText(String.valueOf(feedObjectDataItem.getNumComments()));
            if (feedObjectDataItem.getScore() != null)
                likeCountTv.setText(String.valueOf(feedObjectDataItem.getScore()));

            if (feedObjectDataItem.getPostHint() != null && !feedObjectDataItem.getPreview().getImages().get(0).getResolutions().isEmpty()) {
                int heightestResPreviewPointer = feedObjectDataItem.getPreview().getImages().get(0).getResolutions().size() - 1;
                /*int thumbWidth = rootView.getWidth();
                int thumbHeight = (itemViewWidth / feedObjectDataItem.getPreview().getImages().get(0).getResolutions().get(heightestResPreviewPointer).getWidth())
                        * feedObjectDataItem.getPreview().getImages().get(0).getResolutions().get(heightestResPreviewPointer).getHeight();
                System.out.println("HomeFragment.showFeedItems " + thumbWidth + " " + thumbHeight);*/
                Picasso.get()
                        .load(Utils.unescapeUrl(feedObjectDataItem.getPreview().getImages().get(0).getResolutions().get(heightestResPreviewPointer).getUrl()))
//                        .resize(thumbWidth, thumbHeight)
//                        .centerCrop()
                        .into(thumbIv);
            } else {
                thumbIv.setVisibility(View.GONE);
            }
        }
//        feedAdapter.setData(feedItems);
    }

    @Override
    public void showComments(List<CommentsResponse> commentsResponseData) {
commentsAdapter.setData(commentsResponseData);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getHomeActivity(), message, Toast.LENGTH_SHORT).show();
    }
}
