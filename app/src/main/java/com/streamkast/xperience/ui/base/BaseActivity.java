package com.streamkast.xperience.ui.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.streamkast.xperience.XperienceApp;
import com.streamkast.xperience.di.component.ActivityComponent;
import com.streamkast.reddit.di.component.DaggerActivityComponent;
import com.streamkast.xperience.di.module.ActivityModule;

public abstract class BaseActivity extends AppCompatActivity implements MvpView, BaseFragment.Callback {

    private ActivityComponent mActivityComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(((XperienceApp) getApplication()).getApplicationComponent())
                .build();

        getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);

    }

    public ActivityComponent getActivityComponent() {
        return mActivityComponent;
    }

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void onError(int redId) {

    }

    @Override
    public void onError(String msg) {

    }

    @Override
    public void showMessage(int resId) {

    }

    @Override
    public void showMessage(String msg) {

    }

    @Override
    public boolean isNetworkConnected() {
        return false;
    }

    @Override
    public void hideKeyboard() {

    }
}
