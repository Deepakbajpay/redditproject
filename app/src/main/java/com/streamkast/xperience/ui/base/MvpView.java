package com.streamkast.xperience.ui.base;

import androidx.annotation.StringRes;

public interface MvpView {

    void showLoading();

    void hideLoading();

    void onError(@StringRes int redId);

    void onError(String msg);

    void showMessage(@StringRes int resId);

    void showMessage(String msg);

    boolean isNetworkConnected();

    void hideKeyboard();
}
