package com.streamkast.xperience.ui.home;

import com.streamkast.xperience.data.network.comments.CommentsResponse;
import com.streamkast.xperience.data.network.feed_listing.ListingResponse;
import com.streamkast.xperience.data.network.feed_listing.ListingResponseDataChildrenItem;
import com.streamkast.xperience.data.network.feed_listing.ListingResponseDataItem;
import com.streamkast.xperience.data.source.network.APIClient;
import com.streamkast.xperience.utils.Utils;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragmentPresenterImpl implements HomeFragmentContract.Presenter {

    private HomeFragmentContract.View view;

    public HomeFragmentPresenterImpl(HomeFragmentContract.View view) {
        this.view = view;
    }

    @Override
    public void getFeedItems() {
        if (!Utils.isNetworkAvailable()) {
            view.showMessage("Please check your internet connection and reopen the app");
            return;
        }
        HashMap<String, String> params = new HashMap<>();
        //params.put("g","GLOBAL");
        params.put("limit", "1");
        new APIClient().getApiInterface().getFrontPage(params).enqueue(new Callback<ListingResponse<ListingResponseDataItem<List<ListingResponseDataChildrenItem>>>>() {
            @Override
            public void onResponse(Call<ListingResponse<ListingResponseDataItem<List<ListingResponseDataChildrenItem>>>> call, Response<ListingResponse<ListingResponseDataItem<List<ListingResponseDataChildrenItem>>>> response) {
                if (response.code() == 200) {
                    List<ListingResponseDataChildrenItem> feedList = response.body().getData().getChildren();
                    view.showFeedItems(response.body().getData().getChildren());
                    getFeedComments(feedList.get(0).getData().getSubreddit(),feedList.get(0).getData().getId());
                } else
                    view.showMessage("Something went wrong. Please retry opening app");
            }

            @Override
            public void onFailure(Call<ListingResponse<ListingResponseDataItem<List<ListingResponseDataChildrenItem>>>> call, Throwable t) {
                view.showMessage("Something went wrong. Please retry opening app");
                System.out.println("HomeFragmentPresenterImpl.onFailure " + t.getLocalizedMessage());
            }
        });
    }

    @Override
    public void getFeedComments(String subreddit,String aritcleId) {
        String url = "https://www.reddit.com/r/"+subreddit+"/comments/"+aritcleId+".json";
        HashMap<String, String> params = new HashMap<>();
        params.put("context","1");
        params.put("depth","3");
        params.put("showedits","false");
        params.put("showmore","false");
        params.put("sort","top");
        params.put("threaded","true");
        params.put("truncate","20");
        params.put("limit","20");

        new APIClient().getApiInterface().getComments(url, params).enqueue(new Callback<List<ListingResponse<ListingResponseDataItem<List<CommentsResponse>>>>>() {
            @Override
            public void onResponse(Call<List<ListingResponse<ListingResponseDataItem<List<CommentsResponse>>>>> call, Response<List<ListingResponse<ListingResponseDataItem<List<CommentsResponse>>>>> response) {
                if (response.code() == 200 && response.body().size() > 1) {
                    List<CommentsResponse> commentResponseData = response.body().get(1).getData().getChildren();
                    view.showComments(commentResponseData);
                } else
                    view.showMessage("Something went wrong. Please retry opening app");
            }

            @Override
            public void onFailure(Call<List<ListingResponse<ListingResponseDataItem<List<CommentsResponse>>>>> call, Throwable t) {
                t.printStackTrace();
                view.showMessage("Something went wrong. Please retry opening app");
            }
        });

    }
}
