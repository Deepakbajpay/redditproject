package com.streamkast.xperience.ui.home;

import android.content.Context;

import androidx.fragment.app.Fragment;

public class BaseHomeFragment extends Fragment {
    private HomeActivity homeActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof HomeActivity) {
            homeActivity = ((HomeActivity) context);
        }
    }

    public HomeActivity getHomeActivity(){
        return homeActivity;
    }
}
