package com.streamkast.xperience.di.module;

import android.app.Application;
import android.content.Context;

import com.streamkast.xperience.data.AppDataManager;
import com.streamkast.xperience.data.DataManager;
import com.streamkast.xperience.data.db.AppDbHelper;
import com.streamkast.xperience.data.db.DbHelper;
import com.streamkast.xperience.data.network.ApiHelper;
import com.streamkast.xperience.data.network.AppApiHelper;
import com.streamkast.xperience.data.prefs.AppPreferencesHelper;
import com.streamkast.xperience.data.prefs.PreferencesHelper;
import com.streamkast.xperience.di.ApplicationContext;
import com.streamkast.xperience.di.DatabaseInfo;
import com.streamkast.xperience.di.PreferenceInfo;
import com.streamkast.xperience.utils.AppConstants;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    @Inject
    DataManager mDataManager;
    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName(){return AppConstants.DB_NAME;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName(){return AppConstants.PREF_NAME;}

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager){return appDataManager;}

    @Provides
    @Singleton
    ApiHelper provideApiHelper(AppApiHelper appApiHelper){return appApiHelper;}

    @Provides
    @Singleton
    DbHelper provideDbHelper(AppDbHelper appDbHelper){return appDbHelper;}

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper){return appPreferencesHelper;}

}
