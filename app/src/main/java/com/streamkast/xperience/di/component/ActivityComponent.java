package com.streamkast.xperience.di.component;

import com.streamkast.xperience.di.PerActivity;
import com.streamkast.xperience.di.module.ActivityModule;
import com.streamkast.xperience.ui.home.HomeActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,modules = ActivityModule.class)
public interface ActivityComponent {

    void inject (HomeActivity activity);
}
