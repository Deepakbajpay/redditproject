package com.streamkast.xperience.di.component;

import android.app.Application;
import android.content.Context;

import com.streamkast.xperience.XperienceApp;
import com.streamkast.xperience.di.ApplicationContext;
import com.streamkast.xperience.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(XperienceApp app);

    @ApplicationContext
    Context context();

    Application application();
}
