package com.streamkast.xperience.data.source.network;

import com.streamkast.xperience.data.network.comments.CommentsResponse;
import com.streamkast.xperience.data.network.feed_listing.ListingResponse;
import com.streamkast.xperience.data.network.feed_listing.ListingResponseDataChildrenItem;
import com.streamkast.xperience.data.network.feed_listing.ListingResponseDataItem;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface APIInterface {

    @GET("/r/random.json")
    Call<ListingResponse<ListingResponseDataItem<List<ListingResponseDataChildrenItem>>>> getFrontPage(@QueryMap HashMap<String, String> params);

    @GET
    Call<List<ListingResponse<ListingResponseDataItem<List<CommentsResponse>>>>> getComments(@Url String url, @QueryMap HashMap<String, String> params);


}
