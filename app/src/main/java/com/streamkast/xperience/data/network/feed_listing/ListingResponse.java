package com.streamkast.xperience.data.network.feed_listing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListingResponse<T> {
    @SerializedName("kind")
    @Expose
    private String kind;
    @SerializedName("data")
    @Expose
    private T data;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
