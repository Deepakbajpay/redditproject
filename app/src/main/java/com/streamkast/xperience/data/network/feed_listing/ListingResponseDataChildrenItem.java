package com.streamkast.xperience.data.network.feed_listing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListingResponseDataChildrenItem {

    @SerializedName("kind")
    @Expose
    private String kind;
    @SerializedName("data")
    @Expose
    private FeedObjectDataItem data;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public FeedObjectDataItem getData() {
        return data;
    }

    public void setData(FeedObjectDataItem data) {
        this.data = data;
    }
}
