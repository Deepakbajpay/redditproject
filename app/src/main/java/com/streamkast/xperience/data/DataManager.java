package com.streamkast.xperience.data;

import com.streamkast.xperience.data.db.DbHelper;
import com.streamkast.xperience.data.network.ApiHelper;
import com.streamkast.xperience.data.prefs.PreferencesHelper;

public interface DataManager extends DbHelper, ApiHelper, PreferencesHelper {
}
