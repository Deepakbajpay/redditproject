package com.streamkast.xperience.data.network.comments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommentsResponse {
    @SerializedName("kind")
    @Expose
    private String kind;
    @SerializedName("data")
    @Expose
    private CommentItem data;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public CommentItem  getData() {
        return data;
    }

    public void setData(CommentItem  data) {
        this.data = data;
    }
}
