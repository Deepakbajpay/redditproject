package com.streamkast.xperience.data.network.comments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.streamkast.xperience.data.network.feed_listing.ListingResponseDataItem;

import java.util.List;

public class Replies {

    @SerializedName("kind")
    @Expose
    private String kind;
    @SerializedName("data")
    @Expose
    private ListingResponseDataItem<List<CommentsResponse>> data;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public ListingResponseDataItem<List<CommentsResponse>>  getData() {
        return data;
    }

    public void setData(ListingResponseDataItem<List<CommentsResponse>>  data) {
        this.data = data;
    }
}
