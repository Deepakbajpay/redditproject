package com.streamkast.xperience.utils;

public final class AppConstants {
    public static final String BASE_URL = "https://www.reddit.com";
    public static final String POST_HINT_LINK = "link";

    public static final String DB_NAME = "xperience.db";
    public static final String PREF_NAME = "xperience_pref";
}
