package com.streamkast.xperience;

import android.app.Application;

import com.streamkast.xperience.di.component.ApplicationComponent;
import com.streamkast.reddit.di.component.DaggerApplicationComponent;
import com.streamkast.xperience.di.module.ApplicationModule;

public class XperienceApp extends Application {

    private ApplicationComponent mApplicationComponent;

    private static XperienceApp instance;

    public static XperienceApp getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();

        mApplicationComponent.inject(this);
    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }
}
